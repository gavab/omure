To set up:
# php artisan migrate
# php artisan db:seed --class=CitiesSeeder

To run the updates:
# php artisan schedule:work
# php artisan queue:work
