<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('cities')->insert([
        [
          'name' => 'New York',
          'lon' => -74.006,
          'lat' => 40.7143,
        ], [
          'name' => 'London',
          'lon' => -0.1257,
          'lat' => 51.5085,
        ], [
          'name' => 'Paris',
          'lon' => 2.3488,
          'lat' => 48.8534,
        ], [
          'name' => 'Berlin',
          'lon' => 13.4105,
          'lat' => 52.5244,
        ], [
          'name' => 'Tokyo',
          'lon' => 139.6917,
          'lat' => 35.6895,
        ],
      ]);
    }
}
