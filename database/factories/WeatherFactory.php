<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class WeatherFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'city' => $this->faker->randomElement([
              'London',
              'New York',
              'Paris',
              'Berlin',
              'Tokyo'
            ]),
            'weather' => $this->faker->randomElement([
                json_encode([ 'weather' => 'sunny' ]),
                json_encode([ 'weather' => 'cloudy' ]),
                json_encode([ 'weather' => 'rain' ]),
            ]),
        ];
    }
}
