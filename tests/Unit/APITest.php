<?php

namespace Tests\Unit;

use App\Http\Controllers\WeatherController;
use Tests\TestCase;

class APITest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_api()
    {
        $weather = new WeatherController();
        $resp = $weather->city('London');

        // Make sure we're getting a JSON response
        $this->assertTrue(get_class($resp) == 'Illuminate\Http\JsonResponse');
    }
}
