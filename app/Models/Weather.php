<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'city_id',
        'date',
        'weather',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'weather' => 'object',
        'date' => 'datetime:Y-m-d',
    ];

    /**
     * Return the city for this record
     * @return App\Models\Cities    City information
     */
    public function city() {
      return $this->hasOne(Cities::class);
    }
}
