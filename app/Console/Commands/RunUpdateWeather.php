<?php

namespace App\Console\Commands;

use App\Http\Controllers\WeatherAPIController;
use App\Models\Cities;
use App\Jobs\UpdateWeather;
use Illuminate\Console\Command;

class RunUpdateWeather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update weather';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        dispatch(new UpdateWeather());
        return 0;
    }
}
