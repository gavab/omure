<?php

namespace App\Http\Controllers;

use App\Models\Cities;
use App\Models\Weather;
use App\Providers\LogWeatherChanged;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class WeatherAPIController extends Controller
{
    /**
     * The API key for Open Weather Map
     * @var string
     */
    public $apiKey;

    function __construct()
    {
      // Set API key
      $this->apiKey = env('OWM_API_KEY', '39752c683152f9c8bb23c3c64b6ef93a');
    }

    public function getWeather(string $cityId, int $timestamp = 0)
    {
      // Set time to now
      if ($timestamp === 0) {
        $timestamp = time();
      }

      // If the requested date is in the past we'll use the timemachine URL
      if($timestamp < time()) {
        $url = 'https://api.openweathermap.org/data/2.5/onecall/timemachine';
      } else {
        $url = 'https://api.openweathermap.org/data/2.5/onecall';
      }

      // Get coords for the city
      $city = Cities::find($cityId);
      if (!$city) {
        return false;
      }

      // Build the API query
      $query = http_build_query([
        'dt' => $timestamp,
        'lon' => $city->lon,
        'lat' => $city->lat,
        'units' => 'standard',
        'exclude' => 'minutely',
        'appid' => $this->apiKey,
      ]);

      // Do the request
      try {
        $response = Http::get("${url}?${query}");
      } catch (GuzzleHttp\Exception\ClientException $e) {
        \Log::error($e->getResponse());
        return false;
      }

      // Get the JSON response
      if ($response->getBody()) {
        $weatherData = json_decode($response->getBody());
      } else {
        \Log::error('No response body');
        return false;
      }

      // If the date is for the future, we need to search through the results
      if ($timestamp > time() + 86400) {
        // Go through and check the timestamp matches our date
        foreach($weatherData->daily as $day) {
          if (date('Y-m-d', $day->dt) == date('Y-m-d', $timestamp)) {
            $current = $day;
            continue;
          }
        }
      } else {
        $current = isset($weatherData->current) ? $weatherData->current : [];
      }

      // Get the bits we want
      $ourWeather = [
        'weather' => isset($current) ? $current : [],
        'alerts'  => isset($weatherData->alerts) ? $weatherData->alerts : [],
      ];

      // Save the weather to the database
      $weather = Weather::where('date', date('Y-m-d', $timestamp))
        ->where('city_id', $cityId)
        ->first();
      // Check that a record actually exists
      if ($weather === null) {
        $weather = new Weather();
      }

      // Save the data to the DB
      $weather->city_id = $city->id;
      $weather->date = date('Y-m-d', $timestamp);
      $weather->weather = $ourWeather;

      if ($weather->save()) {
        // Trigger the log event
        LogWeatherChanged::dispatch($weather->id);
        return $weather;
      } else {
        return false;
      }

    }
}
