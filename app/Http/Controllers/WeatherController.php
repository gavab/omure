<?php

namespace App\Http\Controllers;

use App\Models\Weather;
use App\Models\Cities;
use Illuminate\Http\Request;

class WeatherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Create the weather object
        $weather = new Weather();

        // Add the values
        $weather->fill($request->all());
        if ($weather->save()) {
          return response()->json([ 'data' => 'Weather stored successfully' ]);
        } else {
          return response()->json([ 'error' => 'An error occurred' ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json($weather->weather);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update the row
        $weather = Weather::find($id);
        $weather->fill($request->all());
        $weather->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get the latest stored weather for a city
     * @param  string $cityName             Name of the city
     * @uses $_GET['units']                 Units to return
     * @return \Illuminate\Http\Response    Weather object in JSON format
     */
    public function city(string $cityName, string $date = 'now')
    {
        // Get the units
        $units = isset($_GET['units']) ? $_GET['units'] : 'standard';
        switch ($units) {
          case 'metric':
            $units = 'metric';
            break;
          case 'imperial':
            $units = 'imperial';
            break;
          default:
            $units = 'standard';
            break;
        }

        // Cnvert $date to timestamp
        $timestamp = strtotime($date);

        // Get latest result
        $weather = Cities::where('name', $cityName)
          ->join('weather', 'cities.id', '=', 'weather.city_id')
          ->whereDate('weather.date', date('Y-m-d', $timestamp))
          ->orderBy('weather.updated_at', 'desc')
          ->first();

        // Check we have a result
        if ($weather === null) {
          // If there's no result then pull one in
          $city = Cities::where('name', $cityName)->first();
          $weatherApi = new WeatherAPIController();
          $weather = $weatherApi->getWeather($city->id, $timestamp);
          $weatherData = $weather->weather;
        } else {
          $weatherData = json_decode($weather->weather);
        }

        // If there's still no weather then there was a problem
        if ($weather === null) {
          return response()->json([ 'error' => 'There was an error. Please try again later.' ]);
        }

        // Format the results in our own style
        $formattedWeather = $this->format($weatherData, $units);
        return response()->json($formattedWeather);

    }

    /**
     * Convert units and put data into a standard format
     * @param  stdClass $weather               Weather data from the API
     * @param  string   $units                 Unit of measurement
     * @return array                           Formatted array with weather data
     */
    public function format(\stdClass $weather, string $units)
    {
      // Create a new, formatted array
      $formatted = [
        'weather' => [
          'sunrise' => $weather->weather->sunrise,
          'sunset' => $weather->weather->sunset,
          'type' => $weather->weather->weather[0]->main,
          'description' => $weather->weather->weather[0]->description,
          'clouds' => [
            'value' => $weather->weather->clouds,
            'unit' => '%',
          ],
          'uvi' => $weather->weather->uvi,
          'wind_deg' => [
            'value' => $weather->weather->wind_deg,
            'unit' => 'degrees',
          ],
          'humidity' => [
            'value' => $weather->weather->humidity,
            'unit' => '%',
          ],
        ],
        'alerts' => $weather->alerts,
      ];

      // Convert the units
      switch ($units) {
        case 'metric':
          // Open Weather have two ways of returning this
          if (is_object($weather->weather->temp)) {
            $formatted['weather']['temp'] = [
              'value' => (float)$weather->weather->temp->day - 273.15,
              'unit' => '°C',
            ];
          } else {
            $formatted['weather']['temp'] = [
              'value' => (float)$weather->weather->temp - 273.15,
              'unit' => '°C',
            ];
          }
          if (is_object($weather->weather->feels_like)) {
            $formatted['weather']['feels_like'] = [
              'value' => (float)$weather->weather->feels_like->day - 273.15,
              'unit' => '°C',
            ];
          } else {
            $formatted['weather']['feels_like'] = [
              'value' => (float)$weather->weather->feels_like - 273.15,
              'unit' => '°C',
            ];
          }
          $formatted['weather']['dew_point'] = [
            'value' => (float)$weather->weather->dew_point - 273.15,
            'unit' => '°C',
          ];
          $formatted['weather']['pressure'] = [
            'value' => $weather->weather->pressure,
            'unit' => 'hPa',
          ];

          // Where available
          if (isset($weather->weather->visibility)) {
            $formatted['weather']['visibility']  = [
              'value' => $weather->weather->visibility,
              'unit' => 'm',
            ];
          }
          $formatted['weather']['wind_speed']  = [
            'value' => $weather->weather->wind_speed,
            'unit' => 'm/s',
          ];

          // Where available
          if (isset($weather->weather->rain['1h'])) {
            $formatted['weather']['rain']  = [
              'value' => $weather->weather->rain['1h'],
              'unit' => 'mm',
            ];
          }

          // Where available
          if (isset($weather->weather->snow['1h'])) {
            $formatted['weather']['snow']  = [
              'value' => $weather->weather->snow['1h'],
              'unit' => 'mm',
            ];
          }
          break;

        case 'imperial':
          // Open Weather have two ways of returning this
          if (is_object($weather->weather->temp)) {
            $formatted['weather']['temp'] = [
              'value' => ((float)$weather->weather->temp->day - 273.15)  * 9/5 + 32,
              'unit' => '°F',
            ];
          } else {
            $formatted['weather']['temp'] = [
              'value' => ((float)$weather->weather->temp - 273.15) * 9/5 + 32,
              'unit' => '°F',
            ];
          }
          if (is_object($weather->weather->feels_like)) {
            $formatted['weather']['feels_like'] = [
              'value' => ((float)$weather->weather->feels_like->day - 273.15) * 9/5 + 32,
              'unit' => '°F',
            ];
          } else {
            $formatted['weather']['feels_like'] = [
              'value' => ((float)$weather->weather->feels_like - 273.15) * 9/5 + 32,
              'unit' => '°F',
            ];
          }
          $formatted['weather']['dew_point'] = [
            'value' => ((float)$weather->weather->dew_point - 273.15) * 9/5 + 32,
            'unit' => '°F',
          ];
          $formatted['weather']['pressure'] = [
            'value' => (float)$weather->weather->pressure * 0.0145037738,
            'unit' => 'psi',
          ];
          // Where available
          if (isset($weather->weather->visibility)) {
            $formatted['weather']['visibility']  = [
              'value' => (float)$weather->weather->visibility * 0.000621371,
              'unit' => 'mi',
            ];
          }
          $formatted['weather']['wind_speed']  = [
            'value' => (float)$weather->weather->wind_speed * 2.23694,
            'unit' => 'mph',
          ];

          // Where available
          if (isset($weather->weather->rain['1h'])) {
            $formatted['weather']['rain']  = [
              'value' => (float)$weather->weather->rain['1h'] * 0.03937008,
              'unit' => 'in',
            ];
          }

          // Where available
          if (isset($weather->weather->snow['1h'])) {
            $formatted['weather']['snow']  = [
              'value' => (float)$weather->weather->snow['1h'] * 0.03937008,
              'unit' => 'in',
            ];
          }
          break;

        default:
          // Open Weather have two ways of returning this
          if (is_object($weather->weather->temp)) {
            $formatted['weather']['temp'] = [
              'value' => (float)$weather->weather->temp->day,
              'unit' => 'K',
            ];
          } else {
            $formatted['weather']['temp'] = [
              'value' => (float)$weather->weather->temp,
              'unit' => 'K',
            ];
          }
          if (is_object($weather->weather->feels_like)) {
            $formatted['weather']['feels_like'] = [
              'value' => (float)$weather->weather->feels_like->day,
              'unit' => 'K',
            ];
          } else {
            $formatted['weather']['feels_like'] = [
              'value' => (float)$weather->weather->feels_like,
              'unit' => 'K',
            ];
          }
          $formatted['weather']['dew_point'] = [
            'value' => $weather->weather->dew_point,
            'unit' => 'K',
          ];
          $formatted['weather']['pressure'] = [
            'value' => $weather->weather->pressure,
            'unit' => 'hPa',
          ];
          // Where available
          if (isset($weather->weather->visibility)) {
            $formatted['weather']['visibility']  = [
              'value' => $weather->weather->visibility,
              'unit' => 'm',
            ];
          }
          $formatted['weather']['wind_speed']  = [
            'value' => $weather->weather->wind_speed,
            'unit' => 'm',
          ];

          // Where available
          if (isset($weather->weather->rain['1h'])) {
            $formatted['weather']['rain']  = [
              'value' => $weather->weather->rain['1h'],
              'unit' => 'mm',
            ];
          }

          // Where available
          if (isset($weather->weather->snow['1h'])) {
            $formatted['weather']['snow']  = [
              'value' => $weather->weather->snow['1h'],
              'unit' => 'mm',
            ];
          }
          break;
      }

      return $formatted;
    }
}
