<?php

namespace App\Providers;

use App\Providers\WeatherChanged;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogWeatherChanged
{
    /**
     * Handle the event.
     *
     * @param  \App\Providers\WeatherChanged  $event
     * @return void
     */
    public function handle(WeatherChanged $event)
    {
        // Get the weather row
        $weather = Weather::find($event->weatherId)->toArray();

        \Log::info('Weather record updated.');
    }
}
