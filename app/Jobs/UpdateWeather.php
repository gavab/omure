<?php

namespace App\Jobs;

use App\Http\Controllers\WeatherAPIController;
use App\Models\Cities;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateWeather implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Declare WeatherAPIController
        $w = new WeatherAPIController();

        // Get all of the cities
        $cities = Cities::all();

        // Go through the cities
        foreach($cities as $city) {
          $w->getWeather($city->id);
        }

        \Log::info('Weather update is complete');
    }
}
